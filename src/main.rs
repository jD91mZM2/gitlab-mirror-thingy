#[macro_use] extern crate failure;
extern crate git2;
extern crate gitlab;

use git2::{build::RepoBuilder, Cred, ErrorCode, FetchOptions, PushOptions, RemoteCallbacks, Repository, ResetType};
use gitlab::Gitlab;
use std::{
    fs,
    io::{self, prelude::*},
    path::Path,
    process::Command
};

pub const HOST: &str = "gitlab.redox-os.org";
pub const OWNER: u64 = 2;
pub const GITHUB_URL_PREFIX: &str = "git@github.com:redox-os/";
pub const GITHUB_URL_SUFFIX: &str = ".git";
pub const BRANCH: &str = "master";

#[derive(Debug, Fail)]
enum MyError {
    // GitLab returns errors that don't implement Sync
    #[fail(display = "gitlab error: {}", _0)]
    Gitlab(String),
    #[fail(display = "No HEAD for remote origin, even though I just created it.")]
    NoRemoteHead,
    #[fail(display = "HEAD is pointing to nothing. Huh, that's odd.")]
    HeadNullRef,
    #[fail(display = "Path was not valid UTF-8")]
    InvalidPath
}

// Can't clone these libgit2 things
fn new_remote_callback<'a>() -> RemoteCallbacks<'a> {
    let mut remote_callback = RemoteCallbacks::new();
    remote_callback.credentials(|_, _, _| Cred::ssh_key_from_agent("git"));
    remote_callback
}
fn new_fetch_options<'a>() -> FetchOptions<'a> {
    let mut fetch_options = FetchOptions::new();
    fetch_options.remote_callbacks(new_remote_callback());
    fetch_options
}

fn main() -> Result<(), failure::Error> { // Can't use failure::Error because of Send restriction
    print!("Token: ");
    io::stdout().flush()?;

    let mut token = String::new();
    io::stdin().read_line(&mut token)?;

    let basedir = Path::new("gitlab-sync");
    if !basedir.exists() {
        fs::create_dir(basedir)?;
    } else if !basedir.is_dir() {
        eprintln!("File {} is not a directory.", basedir.display());
        eprintln!("I need this path to continue.");
        return Ok(());
    }

    let gitlab = Gitlab::new(HOST, token.trim())
        .map_err(|err| MyError::Gitlab(err.to_string()))?;

    let mut fetch_options = new_fetch_options();
    let mut push_options = PushOptions::new();
    push_options.remote_callbacks(new_remote_callback());

    for project in gitlab.projects().map_err(|err| MyError::Gitlab(err.to_string()))? {
        if project.creator_id.value() != OWNER {
            continue;
        }

        let mut github_url = String::with_capacity(
            GITHUB_URL_PREFIX.len() + project.name.len() + GITHUB_URL_SUFFIX.len()
        );
        github_url.push_str(GITHUB_URL_PREFIX);
        github_url.push_str(&project.name);
        github_url.push_str(GITHUB_URL_SUFFIX);

        let path = basedir.join(&project.name);
        let path_str = path.to_str().ok_or(MyError::InvalidPath)?;

        let repo = if path.exists() {
            println!("Fetching existing {}...", project.name);

            if !path.is_dir() {
                eprintln!("Path {} exists but is not a directory.", path.display());
                return Ok(());
            }

            let repo = Repository::open(&path)?;
            {
                let mut remote = match repo.remote("origin", &project.ssh_url_to_repo) {
                    Ok(remote) => remote,
                    Err(ref err) if err.code() == ErrorCode::Exists => {
                        repo.remote_set_url("origin", &project.ssh_url_to_repo)?;
                        repo.find_remote("origin")?
                    },
                    Err(err) => return Err(err.into())
                };
                remote.fetch(&[BRANCH], Some(&mut fetch_options), None)?;

                let mut glob = String::with_capacity(20 + BRANCH.len());
                glob.push_str("refs/remotes/origin/");
                glob.push_str(BRANCH);

                let mut remote_head_glob = repo.references_glob(&glob)?;
                let remote_head_ref = remote_head_glob.next().ok_or(MyError::NoRemoteHead)?;
                let remote_head_oid = remote_head_ref?.resolve()?.target().ok_or(MyError::HeadNullRef)?;
                let remote_head = repo.find_object(remote_head_oid, None)?;

                repo.reset(&remote_head, ResetType::Hard, None)?;
            }
            repo
        } else {
            println!("Cloning {}...", project.ssh_url_to_repo);

            RepoBuilder::new()
                .fetch_options(new_fetch_options())
                .clone(&project.ssh_url_to_repo, &path)?
        };

        // let mut remote = repo.remote_anonymous(&github_url)?;
        let mut remote = match repo.remote("github", &github_url) {
            Ok(remote) => remote,
            Err(ref err) if err.code() == ErrorCode::Exists => {
                repo.remote_set_url("github", &github_url)?;
                repo.find_remote("github")?
            },
            Err(err) => return Err(err.into())
        };
        remote.fetch(&[BRANCH], Some(&mut fetch_options), None)?;

        println!("Pushing to {}...", github_url);
        // TODO This should be done using libgit2.
        // remote.push(&[BRANCH], Some(&mut push_options))?;
        Command::new("git")
            .args(&["-C", path_str, "push", "github", "master"])
            .status()?;
    }
    Ok(())
}
