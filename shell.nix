with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "gitlab-sync";
  buildInputs = [ pkgconfig openssl libgit2 libssh2 libzip ];
}
